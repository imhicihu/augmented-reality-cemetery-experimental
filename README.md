![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internal-wip](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# Rationale

* This is a collateral project from another one: a virtual 3d reconstruction of a Patagonian cemetery. Once the goal was reached, it enable us to represent it in `augmented reality` mode. 
So, this is a minimal portion of the whole project. 
![cartel.png](https://bitbucket.org/repo/akGo9kM/images/1524102738-cartel.png)

### What is this repository for? ###

* _Process-workflow-checklist_ turning a 2d object and virtually transform it to 3d augmented reality object. 
* Version 1.1

### Procedures ###

* Download the [AR Maker](https://bitbucket.org/imhicihu/ar-cemetery-experimental/downloads/ARMaker.pdf) from our `Downloads` section. Then print it to A4 page size
 ![marker.jpg](https://bitbucket.org/repo/akGo9kM/images/976313947-marker.jpg)
* Download the [ARMedia player for IOS](https://itunes.apple.com/ar/app/armedia-player/id502524441?mt=8) or [ARMedia player for Android](https://play.google.com/store/apps/details?id=com.inglobetechnologies.armedia.player)
* Download the model supplied in the `Downloads` section
* Open this file in ARMedia player. A notification will appear alerting you to a new model is in your library.
![IMG_3912.PNG](https://bitbucket.org/repo/akGo9kM/images/72026351-IMG_3912.PNG)
* Refresh your library. Set `cartel tirado sobre el mar.armedia` as the default model by clicking the `Star` option.
![IMG_3910.PNG](https://bitbucket.org/repo/akGo9kM/images/2222360397-IMG_3910.PNG)
![IMG_3911.PNG](https://bitbucket.org/repo/akGo9kM/images/3611981674-IMG_3911.PNG)
* Point your device to the `ARMaker.pdf` already printed and tap to augment!
* [Here](https://vimeo.com/273370855) can be seen all of this actions
- ![vimeo.gif](https://i.ibb.co/bFjg9W6/AR.gif)
> _Vide_: https://vimeo.com/273370855

### Related repositories ###

* Some repositories linked with this project:
     - [Chapman documentary](https://bitbucket.org/imhicihu/chapman-documentary/src/)
     - [3D Cemetery (Photogrammetry)](https://bitbucket.org/imhicihu/3d-cemetery-photogrammetry/src/) 
     
### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/issues) 
* [![Bitbucket issues](https://img.shields.io/badge/issues-open-green.svg)]()

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/ar-cemetery-experimental/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
     - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Our [Board](https://bitbucket.org/imhicihu/ar-cemetery-experimental/addon/trello/trello-board) is enabled and open to any inquiry. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/ar-cemetery-experimental/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### Copyright ###
![88x31.png](https://bitbucket.org/repo/4pKrXRd/images/3902704043-88x31.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 2.0 Generic License](http://creativecommons.org/licenses/by-sa/2.0/).
