* update rss feed because it change the name of this repo (URGENT)
* update metadata and marker to the 3D model
* update graphics to represent new data added
* ~~add avatar~~
* ~~add Worklow (from the Chapman issue of 2016!)~~
* ~~Aadd .gitignore, .ignoreconfig, .editconfig.~~A
* ~~Add LICENCE~~
* ~~Create `Marker.png` graphic (to eventually place the augmented reality object)~~
* ~~Add `Marker.png` in `Downloads` section~~
* ~~Add metadata to the `ARMaker.pdf` file~~
* ~~add graphics / screen capture from the ipad~~
* ~~Migrate Del.icio.us gathered links to Evernote~~